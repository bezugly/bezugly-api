const mongoose = require('mongoose');
const passport = require('passport');

const User = mongoose.model('User');
const { promisify } = require('es6-promisify');
const { sendError } = require('../handlers/errorHandlers');

exports.validateRegister = async (req, res, next) => {
    req.sanitizeBody('name');
    req.checkBody('name', 'Введите поле name').notEmpty();
    req.checkBody('email', 'Невалидный email').isEmail();
    req.sanitizeBody('email').normalizeEmail({
        gmail_remove_dots: false,
        remove_extension: false,
        gmail_remove_subaddress: false,
    });
    req.checkBody('password', 'Пароль не может быть пустым').notEmpty();
    req.checkBody('passwordConfirm', 'Подтверждение пароля не может быть пустым').notEmpty();
    req.checkBody('passwordConfirm', 'Подтверждение пароля не совпадает с паролем').equals(req.body.password);

    const errors = req.validationErrors();

    if (errors) {
        return sendError({ status: 400, message: 'Ошибка валидации', errors }, req, res);
    }

    const user = await User.findOne({ email: req.body.email });

    if (user) {
        return sendError({
            status: 409,
            message: [{ location: 'internal', param: 'email', msg: 'Такой пользователь уже есть' }],
        }, req, res);
    }

    return next();
};

exports.validateLogin = (req, res, next) => {
    req.checkBody('email', 'Невалидный email').isEmail();
    req.sanitizeBody('email').normalizeEmail({
        gmail_remove_dots: false,
        remove_extension: false,
        gmail_remove_subaddress: false,
    });
    req.checkBody('password', 'Пароль не может быть пустым').notEmpty();

    const errors = req.validationErrors();

    if (errors) {
        return sendError({ status: 400, message: 'Ошибка валидации', errors }, req, res);
    }

    return next();
};

exports.register = async (req, res, next) => {
    const user = new User({ email: req.body.email, name: req.body.name });
    const register = promisify(User.register.bind(User));

    await register(user, req.body.password);

    next();
};

exports.getCurrentUser = (req, res) => {
    const { user } = req;

    if (user) {
        res.json({
            // eslint-disable-next-line
            id: user._id,
            email: user.email,
            name: user.name,
        });
    } else {
        res.json(sendError({ status: 404, message: 'Пользователь на залогинен' }, req, res));
    }
};

exports.login = (req, res, next) => {
    passport.authenticate('local', (authError, user) => {
        if (authError) {
            return next(authError);
        }

        if (!user) {
            return res.json(sendError({ status: 404, message: 'Не знаем такого пользователя' }, req, res));
        }

        return req.logIn(user, (logInError) => {
            if (logInError) {
                return next(logInError);
            }

            return res.json({
                // eslint-disable-next-line
                id: user._id,
                email: user.email,
                name: user.name,
            });
        });
    })(req, res, next);
};

exports.logout = (req, res) => {
    req.logout();
    res.json();
};

exports.isLoggedIn = (req, res, next) => {
    if (req.isAuthenticated()) {
        next();
        return;
    }

    res.json(sendError({ status: 401, message: 'Залогиньтесь' }, req, res));
};
