const mongoose = require('mongoose');

const Post = mongoose.model('Post');
const File = mongoose.model('File');
const Tag = mongoose.model('Tag');
const fs = require('fs');
const multer = require('multer');
const { promisify } = require('es6-promisify');
const translit = require('translit')(require('translit-russian'));
const sluggify = require('slugs');

const readdir = promisify(fs.readdir);

const { sendError } = require('../handlers/errorHandlers');

const getPopulated = (array, populateItem) => {
    const populated = {};
    const list = array.map((listItem) => {
        let newItem = {};

        // eslint-disable-next-line
        if (listItem._doc) {
            // eslint-disable-next-line
            newItem = { ...listItem._doc };
        } else {
            newItem = { ...listItem };
        }

        if (Array.isArray(newItem[populateItem])) {
            // eslint-disable-next-line
            newItem[populateItem].forEach(item => populated[item._id] = item);

            // eslint-disable-next-line
            newItem[populateItem] = newItem[populateItem].map(item => item._id);
        } else if (newItem[populateItem]) {
            // eslint-disable-next-line
            populated[newItem[populateItem]._id] = newItem[populateItem];

            // eslint-disable-next-line
            newItem[populateItem] = newItem[populateItem]._id;
        }

        return newItem;
    });

    return { list, [populateItem]: populated };
};
const getPrevNextPosts = (posts) => {
    if (posts.length === 1) {
        return posts;
    }

    const getPost = post => ({
        title: post.title,
        slug: post.slug,
    });

    if (posts.length === 2) {
        const newPosts = [...posts];
        const firstPost = newPosts[0];
        const secondPost = newPosts[1];

        newPosts[0].prevPost = getPost(secondPost);
        newPosts[1].nextPost = getPost(firstPost);

        return newPosts;
    }

    return posts.map((post, i) => {
        const newPost = { ...post };
        const prevPost = posts[i + 1];
        const nextPost = posts[i - 1];

        if (prevPost) {
            newPost.prevPost = getPost(prevPost);
        }

        if (nextPost) {
            newPost.nextPost = getPost(nextPost);
        }

        return newPost;
    });
};

exports.getPost = async (req, res) => {
    const { id: _id } = req.params;
    const postById = await Post.findOne({ _id, draft: false });

    if (!postById) {
        return sendError({ message: 'Такой заметки нет', status: 404 }, req, res);
    }

    const posts = await Post
        .find({ draft: false })
        .populate('files')
        .populate('ogImage')
        .sort({ created: 'desc' });
    const populatedFiles = getPopulated(posts, 'files');
    const populatedOgImage = getPopulated(populatedFiles.list, 'ogImage');
    const result = getPrevNextPosts(populatedOgImage.list);
    // eslint-disable-next-line
    const post = result.find(item => item._id.toString() === _id.toString());
    const files = {
        ...populatedFiles.files,
        ...populatedOgImage.ogImage,
    };

    return res.json({ post, files });
};

exports.getDraft = async (req, res) => {
    const { id: _id } = req.params;
    const draft = await Post.findOne({ _id, draft: true }).populate('files').populate('ogImage');

    if (!draft) {
        return sendError({ message: 'Такого черновика нет', status: 404 }, req, res);
    }

    const populatedFiles = getPopulated([draft], 'files');
    const populatedOgImage = getPopulated(populatedFiles.list, 'ogImage');
    const files = {
        ...populatedFiles.files,
        ...populatedOgImage.ogImage,
    };

    return res.json({ draft: populatedOgImage.list[0], files });
};

exports.getPosts = async (req, res) => {
    // const { page = 1 } = req.query;
    const query = { draft: false };
    const { slug } = req.query;

    // const limit = 10;
    // const total = await Post.count(query);
    const posts = await Post
        .find(query)
        .populate('files')
        .populate('ogImage')
        // .skip(page * limit - limit)
        // .limit(limit)
        .sort({ created: 'desc' });

    if (slug) {
        const post = posts.find(item => item.slug === slug);

        if (!post) {
            return sendError({ message: 'Такой заметки нет', status: 404 }, req, res);
        }
    }

    const populatedFiles = getPopulated(posts, 'files');
    const populatedOgImage = getPopulated(populatedFiles.list, 'ogImage');
    const files = {
        ...populatedFiles.files,
        ...populatedOgImage.ogImage,
    };
    const result = getPrevNextPosts(populatedOgImage.list);
    const list = slug ? [result.find(item => item.slug === slug)] : result;

    return res.json({ list, files });
};

exports.getDrafts = async (req, res) => {
    // const { page = 1, count } = req.query;
    const query = { draft: true };
    const { draftSlug } = req.query;

    if (draftSlug) {
        query.draftSlug = draftSlug;
    }
    // const limit = 10;
    // const total = await Post.count(query);
    const drafts = await Post
        .find(query)
        .populate('files')
        .populate('ogImage')
        // .skip(page * limit - limit)
        // .limit(limit)
        .sort({ updated: 'desc' });

    if (draftSlug && !drafts.length) {
        return sendError({ message: 'Такой заметки нет', status: 404 }, req, res);
    }

    const populatedFiles = getPopulated(drafts, 'files');
    const populatedOgImage = getPopulated(populatedFiles.list, 'ogImage');
    const files = {
        ...populatedFiles.files,
        ...populatedOgImage.ogImage,
    };

    return res.json({ list: populatedOgImage.list, files });
};

exports.createPost = async (req, res) => {
    // Отправлять айдишник тега
    // populate('tag', 'name (какие поля вытащить)')
    const post = await (new Post({ ...req.body, draft: true })).save();

    res.json(post);
};

exports.editPost = async (req, res) => {
    // eslint-disable-next-line
    const _id = req.params.id;

    // eslint-disable-next-line
    if (req.body._id && _id !== req.body._id) {
        return sendError({ message: 'Нельзя обновить id поста', status: 400 }, req, res);
    }

    const post = await Post.findById(_id);

    if (!post) {
        return sendError({ message: 'Такой заметки нет', status: 404 }, req, res);
    }

    post.set(req.body);

    const updatedPost = await post.save();

    return res.json(updatedPost);
};

exports.removePost = async (req, res) => {
    const post = await Post.findById(req.params.id);

    if (!post) {
        return sendError({ message: 'Такой заметки нет', status: 404 }, req, res);
    }

    const { files } = post;
    const filesRequests = [];

    for (let i = 0; i < files.length; i += 1) {
        // eslint-disable-next-line
        const file = await File.findById(files[i]);
        // eslint-disable-next-line
        const postsWithFiles = await Post.find({ files: file });

        if (file && postsWithFiles.length <= 1) {
            file.set({ deleted: true });

            filesRequests.push(file.save());
        }
    }

    await Promise.all(filesRequests);
    await post.remove();

    // eslint-disable-next-line
    return res.json(post);
};

exports.uploadImage = multer({
    fileFilter(req, file, next) {
        const isPhoto = file.mimetype.startsWith('image/');

        if (isPhoto) {
            next(null, true);
        } else {
            next({ status: 400, message: 'Такой файл не поддерживается' }, false);
        }
    },
    storage: multer.diskStorage({
        destination(req, file, next) {
            next(null, process.env.UPLOADS_FOLDER);
        },
        filename: async (req, file, next) => {
            const { mimetype, originalname } = file;
            const folder = process.env.UPLOADS_FOLDER;
            const name = sluggify(translit(originalname.split('.').slice(0, -1).join('.')));
            const extension = mimetype.split('/')[1];
            const imageRegexp = new RegExp(`^(${name})((-[0-9]*)?).(${extension})`);
            let fileName = `${name}.${extension}`;

            const images = await readdir(folder);

            if (images) {
                const sameImagesNames = images.filter(image => imageRegexp.test(image));
                const count = sameImagesNames.length;

                if (count) {
                    fileName = `${name}-${count + 1}.${extension}`;
                }
            }

            req.uploadFile = {
                fileName,
                mimetype,
                originalname,
            };

            next(null, fileName);
        },
    }),
})
    .single('photo');

exports.createFile = async (req, res, next) => {
    if (!req.uploadFile) {
        return next();
    }

    const {
        fileName,
        src,
        mimetype,
        originalname,
    } = req.uploadFile;

    const file = await (new File({
        name: fileName,
        src,
        mimeType: mimetype,
        originalName: originalname,
    })).save();

    return res.json(file);
};

exports.getFile = async (req, res) => {
    const { id: _id } = req.params;
    const file = await File.findOne({ _id });

    if (!file) {
        return sendError({ message: 'Такого файла нет', status: 404 }, req, res);
    }

    return res.json(file);
};

exports.editFile = async (req, res) => {
    // eslint-disable-next-line
    const _id = req.params.id;

    // eslint-disable-next-line
    if (req.body._id && _id !== req.body._id) {
        return sendError({ message: 'Нельзя обновить id файла', status: 400 }, req, res);
    }

    const file = await File.findById(_id);

    if (!file) {
        return sendError({ message: 'Такого файла нет', status: 404 }, req, res);
    }

    file.set(req.body);

    const updatedFile = await file.save();

    return res.json(updatedFile);
};

exports.searchPost = async (req, res) => {
    if (!req.query.q) {
        res.json([]);
    }

    const posts = await Post
        .find({ $text: { $search: req.query.q } }, { score: { $meta: 'textScore' } })
        .sort({ score: { $meta: 'textScore' } });

    res.json(posts);
};

exports.getTags = async (req, res) => {
    // TODO: отсортировать по частоте использования
    const tags = await Tag.find();

    res.json(tags);
};

exports.getPostsByTag = async (req, res) => {
    const posts = await Post.find({ tag: req.params.id });

    res.json(posts);
};

exports.getPostSlug = async (req, res) => {
    const { slug } = req.params;

    const post = await Post.findOne({ draft: false, slug });

    if (!post) {
        return sendError({ message: 'Такого слага нет', status: 404 }, req, res);
    }

    return res.json(post.slug);
};

// exports.getStoreBySlug = async (req, res, next) => {
//   const store = await Store.findOne({ slug: req.params.slug }).populate('author reviews');
//   if (!store) return next();
//   res.render('store', { store, title: store.name });
// };

// exports.getStoresByTag = async (req, res) => {
//   const tag = req.params.tag;
//   const tagQuery = tag || { $exists: true, $ne: [] };

//   const tagsPromise = Store.getTagsList();
//   const storesPromise = Store.find({ tags: tagQuery });
//   const [tags, stores] = await Promise.all([tagsPromise, storesPromise]);


//   res.render('tag', { tags, title: 'Tags', tag, stores });
// };
