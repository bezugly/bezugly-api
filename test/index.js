require('dotenv').config();

const mongoose = require('mongoose');
const request = require('supertest');

mongoose.Promise = global.Promise;

require('../models/blog/Post');
require('../models/blog/File');
require('../models/blog/Tag');
require('../models/User');

const app = require('../app');

const authenticatedUser = request.agent(app);
const blogTests = require('./blog');
const userTests = require('./user');

describe('Тестирование', () => {
    before((done) => {
        if (mongoose.connection.db) {
            done();
        }

        mongoose.connect(process.env.DATABASE, { useNewUrlParser: true }, () => (
            authenticatedUser
                .post('/v1/user/login')
                .set('Accept', 'application/json')
                .send({
                    email: process.env.TEST_USER,
                    password: process.env.TEST_USER_PASSWORD,
                })
                .then(() => done())
        ));
    });

    blogTests(app, authenticatedUser);
    userTests(app, authenticatedUser);

    after((done) => {
        mongoose.connection.close(true, done);
    });
});
