const assert = require('assert');
const request = require('supertest');
const User = require('../../models/User');

const name = 'Димка';
const email = 'test@test1.ru';
const password = '123';
const passwordConfirm = '123';
const addUser = app => (
    new Promise(resolve => (
        request(app)
            .post('/v1/user/register')
            .send({
                name,
                password,
                passwordConfirm,
                email,
            })
            .set('Content-Type', 'application/json')
            .then(resolve)
    ))
);
const removeUser = () => (
    new Promise(resolve => User.findOne({ email }).then(resolve))
        .then(user => user.remove())
);
const userTests = function (app, authenticatedUser) {
    return (
        describe('Пользователи', () => {
            describe('Регистрация', () => {
                describe('POST /v1/user/register', () => {
                    describe('Позитивные', () => {
                        it('Зарегистрировался по name, email и паролю', () => (
                            request(app)
                                .post('/v1/user/register')
                                .send({
                                    name,
                                    password,
                                    passwordConfirm,
                                    email,
                                })
                                .set('Content-Type', 'application/json')
                                .expect(200)
                                .expect('Content-Type', /json/)
                                .then(({ body }) => assert.strictEqual(body.email, email))
                        ));
                    });

                    describe('Негативные', () => {
                        it('Возвращает 415, если Content-Type не application/json', () => (
                            request(app)
                                .post('/v1/user/register')
                                .set('Content-Type', 'text/plain')
                                .expect(415)
                        ));

                        it('Возвращает 400, если ничего не передать', () => (
                            request(app)
                                .post('/v1/user/register')
                                .send({})
                                .set('Content-Type', 'application/json')
                                .expect(400)
                                .then(({ body }) => {
                                    assert.ok(body.errors.length);
                                })
                        ));

                        it('Возвращает 400, если не передать name', () => (
                            request(app)
                                .post('/v1/user/register')
                                .send({ password, passwordConfirm, email })
                                .set('Content-Type', 'application/json')
                                .expect(400)
                                .then(({ body }) => {
                                    assert.ok(body.errors.length === 1);
                                    assert.strictEqual(body.errors[0].param, 'name');
                                })
                        ));

                        it('Возвращает 400, если не передать email', () => (
                            request(app)
                                .post('/v1/user/register')
                                .send({ name, password, passwordConfirm })
                                .set('Content-Type', 'application/json')
                                .expect(400)
                                .then(({ body }) => {
                                    assert.ok(body.errors.length === 1);
                                    assert.strictEqual(body.errors[0].param, 'email');
                                })
                        ));

                        it('Возвращает 400, если не передать password', () => (
                            request(app)
                                .post('/v1/user/register')
                                .send({ name, passwordConfirm, email })
                                .set('Content-Type', 'application/json')
                                .expect(400)
                                .then(({ body }) => {
                                    assert.ok(body.errors.length);
                                })
                        ));

                        it('Возвращает 400, если не передать passwordConfirm', () => (
                            request(app)
                                .post('/v1/user/register')
                                .send({ name, password, email })
                                .set('Content-Type', 'application/json')
                                .expect(400)
                                .then(({ body }) => {
                                    assert.ok(body.errors.length);
                                })
                        ));

                        it('Возвращает 400, если password и passwordConfirm не совпадают', () => (
                            request(app)
                                .post('/v1/user/register')
                                .send({
                                    name,
                                    password,
                                    passwordConfirm: `${password}1`,
                                    email,
                                })
                                .set('Content-Type', 'application/json')
                                .expect(400)
                                .then(({ body }) => {
                                    assert.ok(body.errors.length === 1);
                                    assert.strictEqual(body.errors[0].param, 'passwordConfirm');
                                })
                        ));

                        it('Возвращает 400, если передать невалидный email', () => (
                            request(app)
                                .post('/v1/user/register')
                                .send({
                                    name,
                                    password,
                                    passwordConfirm,
                                    email: 'dass.ru',
                                })
                                .set('Content-Type', 'application/json')
                                .expect(400)
                                .then(({ body }) => {
                                    assert.ok(body.errors.length === 1);
                                    assert.strictEqual(body.errors[0].param, 'email');
                                })
                        ));

                        it('Возвращает 409, если email уже есть в базе', () => (
                            request(app)
                                .post('/v1/user/register')
                                .send({
                                    name,
                                    password,
                                    passwordConfirm,
                                    email,
                                })
                                .set('Content-Type', 'application/json')
                                .expect(409)
                        ));
                    });

                    after(removeUser);
                });
            });

            describe('Авторизация', () => {
                describe('POST /v1/user/login', () => {
                    before(() => addUser(app));

                    describe('Позитивные', () => {
                        it('Логинит пользователя по email и password', () => (
                            request(app)
                                .post('/v1/user/login')
                                .send({ email, password })
                                .set('Content-Type', 'application/json')
                                .expect(200)
                                .expect('Content-Type', /json/)
                                .then(({ body: user }) => {
                                    assert.strictEqual(user.email, email);
                                })
                        ));
                    });

                    describe('Негативные', () => {
                        it('Возвращает 415, если Content-Type не application/json', () => (
                            request(app)
                                .post('/v1/user/login')
                                .set('Content-Type', 'text/plain')
                                .expect(415)
                        ));

                        it('Возвращает 400, если ничего не передать', () => (
                            request(app)
                                .post('/v1/user/login')
                                .send({})
                                .set('Content-Type', 'application/json')
                                .expect(400)
                        ));

                        it('Возвращает 400, если не передал email', () => (
                            request(app)
                                .post('/v1/user/login')
                                .send({ password })
                                .set('Content-Type', 'application/json')
                                .expect(400)
                        ));

                        it('Возвращает 400, если не передал password', () => (
                            request(app)
                                .post('/v1/user/login')
                                .send({ email })
                                .set('Content-Type', 'application/json')
                                .expect(400)
                        ));

                        it('Возвращает 400, если передал невалидный email', () => (
                            request(app)
                                .post('/v1/user/login')
                                .send({ email: 'dasss.ru', password })
                                .set('Content-Type', 'application/json')
                                .expect(400)
                        ));

                        it('Возвращает 404, если не нашел пользователя', () => (
                            request(app)
                                .post('/v1/user/login')
                                .send({ email: 'vasya_test@rambler.ru', password })
                                .set('Content-Type', 'application/json')
                                .expect(404)
                        ));
                    });

                    after(removeUser);
                });
            });

            describe('Завершение сеанса', () => {
                describe('GET /v1/user/logout', () => {
                    before(() => addUser(app));

                    describe('Позитивные', () => {
                        it('Завершает сеанс пользователя', () => (
                            request(app)
                                .get('/v1/user/logout')
                                .set('Content-Type', 'application/json')
                                .expect(200)
                                .expect('Content-Type', /json/)
                        ));
                    });

                    after(removeUser);
                });
            });

            describe('Получение текущего пользователя', () => {
                describe('GET /v1/user', () => {
                    describe('Позитивные', () => {
                        it('Отдает текущего пользователя', () => (
                            authenticatedUser
                                .get('/v1/user')
                                .expect(200)
                                .expect('Content-Type', /json/)
                                .then(({ body: user }) => assert.ok(user.id))
                        ));
                    });

                    describe('Негативные', () => {
                        it('Возвращает 404, если пользователь не залогинен', () => (
                            request(app)
                                .get('/v1/user')
                                .expect(404)
                        ));
                    });
                });
            });
        })
    );
};

module.exports = userTests;
