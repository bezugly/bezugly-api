const draftsTests = require('./drafts');
const postsTests = require('./posts');
const imagesTests = require('./images');

const blogTests = function (app, authenticatedUser) {
    return (
        describe('Блог', () => {
            postsTests(app, authenticatedUser);
            draftsTests(app, authenticatedUser);
            imagesTests(app, authenticatedUser);
        })
    );
};

module.exports = blogTests;
