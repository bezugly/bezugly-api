const assert = require('assert');
const request = require('supertest');

const draftsTests = function (app, authenticatedUser) {
    return (
        describe('Черновики', () => {
            describe('GET /v1/blog/drafts', () => {
                describe('Позитивные', () => {
                    it('Отдает список черновиков', () => (
                        authenticatedUser
                            .get('/v1/blog/drafts')
                            .expect(200)
                            .expect('Content-Type', /json/)
                            .then(({ body }) => {
                                assert.ok(
                                    Array.isArray(body.list) && body.list.every(post => post.draft),
                                );
                            })
                    ));

                    it('Возвращает черновик с параметром slug', () => {
                        const draftSlug = 'moy-slag-dlya-chernovika';

                        return authenticatedUser
                            .get(`/v1/blog/drafts?draftSlug=${draftSlug}`)
                            .expect(200)
                            .expect('Content-Type', /json/)
                            .then(({ body }) => {
                                assert.ok(body.list.length === 1);
                                assert.ok(body.list[0].draftSlug === draftSlug);
                            });
                    });
                });

                describe('Негативные', () => {
                    it('Возвращает 401, если стучится незареганный пользователь', () => (
                        request(app)
                            .get('/v1/blog/drafts?draftSlug=abra151k4a32da1bra')
                            .expect(401)
                    ));

                    it('Возвращает 404, если черновика нет', () => (
                        authenticatedUser
                            .get('/v1/blog/drafts?draftSlug=abra151k4a32da1bra')
                            .expect(404)
                    ));
                });
            });

            describe('GET /v1/blog/drafts/:id', () => {
                describe('Позитивные', () => {
                    it('Возвращает черновик, если передать айди', () => (
                        authenticatedUser
                            .get('/v1/blog/drafts')
                            .then(({ body: drafts }) => {
                                // eslint-disable-next-line
                                const draftId = drafts.list[0]._id;

                                return authenticatedUser
                                    .get(`/v1/blog/drafts/${draftId}`)
                                    .then(({ body }) => {
                                        // eslint-disable-next-line
                                        assert.ok(body.draft._id === draftId);
                                    });
                            })
                    ));
                });

                describe('Негативные', () => {
                    it('Возвращает 401, если стучится незареганный пользователь', () => (
                        authenticatedUser
                            .get('/v1/blog/drafts')
                            .then(({ body: drafts }) => {
                                // eslint-disable-next-line
                                const draftId = drafts.list[0]._id;

                                return request(app)
                                    .get(`/v1/blog/drafts/${draftId}`)
                                    .expect(401);
                            })
                    ));

                    it('Возвращает 404, если поста нет', () => (
                        authenticatedUser
                            .get('/v1/blog/posts')
                            .then(({ body }) => {
                                // eslint-disable-next-line
                                const postId = body.list[0]._id;

                                return authenticatedUser
                                    .get(`/v1/blog/drafts/${postId}`)
                                    .expect(404);
                            })
                    ));

                    it('Возвращает 400, если передать не MongoId', () => (
                        authenticatedUser
                            .get('/v1/blog/drafts/123')
                            .expect(400)
                    ));
                });
            });
        })
    );
};

module.exports = draftsTests;
