const path = require('path');
const request = require('supertest');
const assert = require('assert');
const slugs = require('slugs');
const translit = require('translit')(require('translit-russian'));

const postsTests = function (app, authenticatedUser) {
    describe('Посты', () => {
        describe('GET /v1/blog/posts', () => {
            describe('Позитивные', () => {
                it('Отдает список постов', () => (
                    request(app)
                        .get('/v1/blog/posts')
                        .expect(200)
                        .expect('Content-Type', /json/)
                        .then((response) => {
                            const { body } = response;
                            assert.ok(
                                Array.isArray(body.list) && body.list.every(post => !post.draft),
                            );
                        })
                ));

                it('Возвращает пост с параметром slug', () => {
                    const slug = 'moy-slag';

                    return authenticatedUser
                        .post('/v1/blog/posts')
                        .send({ slug })
                        .set('Content-Type', 'application/json')
                        .then(({ body }) => (
                            authenticatedUser
                                // eslint-disable-next-line
                                .patch(`/v1/blog/posts/${body._id}`)
                                .send({ draft: false })
                        ))
                        .then(() => (
                            request(app)
                                .get(`/v1/blog/posts?slug=${slug}`)
                                .expect(200)
                                .expect('Content-Type', /json/)
                        ))
                        .then(({ body }) => {
                            assert.ok(body.list.length === 1);
                            assert.ok(body.list[0].slug === slug);
                        });
                });
            });

            describe('Негативные', () => {
                it('Возвращает 404, если поста нет', () => (
                    request(app)
                        .get('/v1/blog/posts?slug=abra151k4a32da1bra')
                        .expect(404)
                ));
            });
        });

        describe('POST /v1/blog/posts', () => {
            describe('Позитивные', () => {
                it('Создает черновик, если ничего не передать', () => (
                    authenticatedUser
                        .post('/v1/blog/posts')
                        .set('Content-Type', 'application/json')
                        .expect(200)
                        .expect('Content-Type', /json/)
                        .then(({ body }) => {
                            assert.strictEqual(body.draft, true);
                        })
                ));

                it('Создает черновик, если передать только заголовок', () => {
                    const title = 'Интересный заголовок';

                    return authenticatedUser
                        .post('/v1/blog/posts')
                        .send({ title })
                        .set('Content-Type', 'application/json')
                        .expect(200)
                        .expect('Content-Type', /json/)
                        .then(({ body }) => {
                            assert.strictEqual(body.title, title);
                            assert.strictEqual(body.draft, true);
                        });
                });

                it('Создает черновик, если передать только контент', () => {
                    const content = 'Интересный контент';

                    return authenticatedUser
                        .post('/v1/blog/posts')
                        .send({ content })
                        .set('Content-Type', 'application/json')
                        .expect(200)
                        .expect('Content-Type', /json/)
                        .then(({ body }) => {
                            assert.strictEqual(body.content, content);
                            assert.strictEqual(body.draft, true);
                        });
                });

                it('Создает черновик, если передать только слаг черновика', () => {
                    const draftSlug = 'draftSlug';

                    return authenticatedUser
                        .post('/v1/blog/posts')
                        .send({ draftSlug })
                        .set('Content-Type', 'application/json')
                        .expect(200)
                        .expect('Content-Type', /json/)
                        .then(({ body }) => {
                            assert.strictEqual(body.draft, true);
                        });
                });

                it('Создает черновик, если передать только слаг', () => {
                    const slug = 'userSlug';

                    return authenticatedUser
                        .post('/v1/blog/posts')
                        .send({ slug })
                        .set('Content-Type', 'application/json')
                        .expect(200)
                        .expect('Content-Type', /json/)
                        .then(({ body }) => {
                            assert.strictEqual(body.draft, true);
                        });
                });

                it('Создает черновик, если передать только дату', () => {
                    const created = Date.now();

                    return authenticatedUser
                        .post('/v1/blog/posts')
                        .send({ created })
                        .set('Content-Type', 'application/json')
                        .expect(200)
                        .expect('Content-Type', /json/)
                        .then(({ body }) => {
                            assert.strictEqual(body.draft, true);
                        });
                });

                it('Создает черновик, если передать только файлы', () => (
                    authenticatedUser
                        .post('/v1/blog/images')
                        .attach('photo', path.join(__dirname, 'fixtures', 'cake.jpg'))
                        .then(({ body: image }) => (
                            authenticatedUser
                                .post('/v1/blog/posts')
                                // eslint-disable-next-line
                                .send({ files: [image._id] })
                                .set('Content-Type', 'application/json')
                                .expect(200)
                                .expect('Content-Type', /json/)
                                .then(({ body }) => {
                                    assert.strictEqual(body.files.length, 1);
                                    assert.strictEqual(body.draft, true);
                                })
                        ))
                ));

                it('Создает черновик, если передать все данные', () => {
                    const requestBody = {
                        title: 'Моя шикарная статья',
                        content: 'Мой шикарный контент',
                        slug: 'my-sexy-slug',
                        draftSlug: 'my-sexy-slug',
                        draft: false,
                        tags: [],
                        files: [],
                        created: Date.now(),
                    };

                    return authenticatedUser
                        .post('/v1/blog/posts')
                        .send(requestBody)
                        .set('Content-Type', 'application/json')
                        .expect(200)
                        .expect('Content-Type', /json/)
                        .then((response) => {
                            const { body } = response;

                            assert.strictEqual(body.title, requestBody.title);
                            assert.strictEqual(body.content, requestBody.content);
                            assert.strictEqual(body.draft, true);
                        });
                });
            });

            describe('Негативные', () => {
                it('Возвращает 401, если стучится незареганный пользователь', () => (
                    request(app)
                        .post('/v1/blog/posts')
                        .expect(401)
                ));

                it('Возвращает 415, если Content-Type не application/json', () => (
                    authenticatedUser
                        .post('/v1/blog/posts')
                        .send('invalid text')
                        .set('Content-Type', 'text/plain')
                        .expect(415)
                ));

                it('Создает черновик, если передать draft: false', () => (
                    authenticatedUser
                        .post('/v1/blog/posts')
                        .send({ draft: false })
                        .set('Content-Type', 'application/json')
                        .expect(200)
                        .expect('Content-Type', /json/)
                        .then(({ body }) => {
                            assert.strictEqual(body.draft, true);
                        })
                ));

                it('Создает черновик с новым уникальным draftSlug, если передать существующий draftSlug', () => {
                    const draftSlug = 'new-slug';

                    return authenticatedUser
                        .post('/v1/blog/posts')
                        .send({ draftSlug })
                        .set('Content-Type', 'application/json')
                        .expect(200)
                        .expect('Content-Type', /json/)
                        .then(({ body: oldPost }) => (
                            authenticatedUser
                                .post('/v1/blog/posts')
                                .send({ draftSlug })
                                .set('Content-Type', 'application/json')
                                .expect(200)
                                .expect('Content-Type', /json/)
                                .then(({ body: newPost }) => {
                                    assert.ok(oldPost.draftSlug !== newPost.draftSlug);
                                })
                        ));
                });

                it('Переводит slug и defaultSlug на английский, если передать их на русском', () => {
                    const slug = 'мой слаг';
                    const draftSlug = 'мой слаг для черновика';
                    const convertedSlug = slugs(translit(slug));
                    const convertedDraftSlug = slugs(translit(draftSlug));

                    return authenticatedUser
                        .post('/v1/blog/posts')
                        .send({ draftSlug, slug })
                        .set('Content-Type', 'application/json')
                        .expect(200)
                        .expect('Content-Type', /json/)
                        .then(({ body }) => {
                            const regexp = new RegExp(convertedDraftSlug);

                            assert.strictEqual(body.slug, convertedSlug);
                            assert.ok(regexp.test(body.draftSlug));
                        });
                });

                it('Возвращает 400, если передать не MongoId файлов', () => (
                    authenticatedUser
                        .post('/v1/blog/posts')
                        .send({ files: [13, 21, -1] })
                        .set('Content-Type', 'application/json')
                        .expect(400)
                ));

                // it('Выводит ошибку, если передать невалидные теги', () => {
                //     return authenticatedUser
                //         .post('/v1/blog/posts')
                //         .send({ files: [13, 21, -1] })
                //         .set('Content-Type', 'application/json')
                //         .expect(400);
                // });

                it('Возвращает 400, если передать невалидный reference', () => (
                    authenticatedUser
                        .post('/v1/blog/posts')
                        .send({ reference: 13 })
                        .set('Content-Type', 'application/json')
                        .expect(400)
                ));

                it('Создает текущую дату, если передать дату из будущего', () => {
                    const futureDate = Date.now() + 10000000;

                    return authenticatedUser
                        .post('/v1/blog/posts')
                        .send({ created: futureDate })
                        .set('Content-Type', 'application/json')
                        .expect(200)
                        .expect('Content-Type', /json/)
                        .then(({ body }) => {
                            assert.ok(Date.parse(body.created) < futureDate);
                        });
                });
            });
        });

        describe('GET /v1/blog/posts/:id', () => {
            describe('Позитивные', () => {
                it('Возвращает пост, если передать айди', () => (
                    request(app)
                        .get('/v1/blog/posts')
                        .then(({ body: posts }) => {
                            // eslint-disable-next-line
                            const postId = posts.list[0]._id;

                            return request(app)
                                .get(`/v1/blog/posts/${postId}`)
                                .then(({ body }) => {
                                    // eslint-disable-next-line
                                    assert.ok(body.post._id === postId);
                                });
                        })
                ));
            });

            describe('Негативные', () => {
                it('Возвращает 404, если поста нет', () => (
                    authenticatedUser
                        .get('/v1/blog/drafts')
                        .then(({ body }) => {
                            // eslint-disable-next-line
                            const postId = body.list[0]._id;

                            return request(app)
                                .get(`/v1/blog/posts/${postId}`)
                                .expect(404);
                        })
                ));

                it('Возвращает 400, если передать не MongoId', () => (
                    request(app)
                        .get('/v1/blog/posts/123')
                        .expect(400)
                ));
            });
        });

        describe('PATCH /v1/blog/posts/:id', () => {
            describe('Позитивные', () => {
                it('Записывает изменения в посте', () => (
                    authenticatedUser
                        .get('/v1/blog/drafts')
                        .then(({ body }) => {
                            // eslint-disable-next-line
                            const postId = body.list[0]._id;
                            const data = {
                                title: 'Мой постик',
                                content: 'Приветульк',
                                draftSlug: 'my-new-draft-slug',
                                slug: 'my-new-slug',
                                draft: false,
                                files: [],
                                created: Date.now(),
                            };

                            return { postId, data };
                        })
                        .then(({ postId, data }) => (
                            authenticatedUser
                                .patch(`/v1/blog/posts/${postId}`)
                                .send(data)
                                .set('Content-Type', 'application/json')
                                .expect(200)
                                .expect('Content-Type', /json/)
                                .then(({ body }) => {
                                    // eslint-disable-next-line
                                    assert.ok(body._id === postId);
                                })
                        ))
                ));
            });

            describe('Негативные', () => {
                it('Возвращает 404, если поста нет', () => (
                    authenticatedUser
                        .patch('/v1/blog/posts/5c75b29a114e8bd3d1f40229')
                        .set('Content-Type', 'application/json')
                        .expect(404)
                ));

                it('Возвращает 400, если передать не MongoId', () => (
                    authenticatedUser
                        .patch('/v1/blog/posts/1231')
                        .set('Content-Type', 'application/json')
                        .expect(400)
                ));

                it('Возвращает 401, если стучится незареганный пользователь', () => (
                    request(app)
                        .patch('/v1/blog/posts/5c75b29a114e8bd3d1f40229')
                        .set('Content-Type', 'application/json')
                        .expect(401)
                ));

                it('Возвращает 415, если Content-Type не application/json', () => (
                    authenticatedUser
                        .get('/v1/blog/drafts')
                        .then(({ body }) => {
                            // eslint-disable-next-line
                            const postId = body.list[0]._id;

                            return authenticatedUser
                                .patch(`/v1/blog/posts/${postId}`)
                                .expect(415);
                        })
                ));

                it('Возвращает 400, если попробовать изменить id поста', () => (
                    authenticatedUser
                        .get('/v1/blog/drafts')
                        .then(({ body }) => {
                            // eslint-disable-next-line
                            const postId = body.list[0]._id;

                            return authenticatedUser
                                .patch(`/v1/blog/posts/${postId}`)
                                .send({ _id: '5c75ba5a190bfcd40853747f' })
                                .set('Content-Type', 'application/json')
                                .expect(400);
                        })
                ));

                it('Создает draftSlug, если передать пустой', () => (
                    authenticatedUser
                        .get('/v1/blog/drafts')
                        .then((response) => {
                            const { body: drafts } = response;
                            // eslint-disable-next-line
                            const postId = drafts.list[0]._id;

                            return authenticatedUser
                                .patch(`/v1/blog/posts/${postId}`)
                                .send({ draftSlug: '' })
                                .set('Content-Type', 'application/json')
                                .expect(200)
                                .expect('Content-Type', /json/)
                                .then(({ body }) => {
                                    assert.ok(Boolean(body.draftSlug.length));
                                });
                        })
                ));

                it('Создает slug, если передать пустой', () => (
                    authenticatedUser
                        .get('/v1/blog/drafts')
                        .then(({ body: drafts }) => {
                            // eslint-disable-next-line
                            const postId = drafts.list[0]._id;

                            return authenticatedUser
                                .patch(`/v1/blog/posts/${postId}`)
                                .send({ slug: '' })
                                .set('Content-Type', 'application/json')
                                .expect(200)
                                .expect('Content-Type', /json/)
                                .then(({ body }) => {
                                    assert.ok(Boolean(body.slug.length));
                                });
                        })
                ));

                it('Переводит slug и defaultSlug на английский, если передать их на русском', () => {
                    const slug = 'мой слаг';
                    const draftSlug = 'мой слаг для черновика';
                    const convertedSlug = slugs(translit(slug));
                    const convertedDraftSlug = slugs(translit(draftSlug));

                    return authenticatedUser
                        .get('/v1/blog/drafts')
                        .then(({ body: drafts }) => {
                            // eslint-disable-next-line
                            const postId = drafts.list[0]._id;

                            return authenticatedUser
                                .patch(`/v1/blog/posts/${postId}`)
                                .send({ draftSlug, slug })
                                .set('Content-Type', 'application/json')
                                .expect(200)
                                .expect('Content-Type', /json/)
                                .then(({ body }) => {
                                    const regexp = new RegExp(convertedDraftSlug);

                                    assert.strictEqual(body.slug, convertedSlug);
                                    assert.ok(regexp.test(body.draftSlug));
                                });
                        });
                });

                it('Создает текущую дату, если передать дату из будущего', () => {
                    const futureDate = Date.now() + 10000000;

                    return authenticatedUser
                        .get('/v1/blog/drafts')
                        .then(({ body: drafts }) => {
                            // eslint-disable-next-line
                            const postId = drafts.list[0]._id;

                            return authenticatedUser
                                .patch(`/v1/blog/posts/${postId}`)
                                .send({ created: futureDate })
                                .set('Content-Type', 'application/json')
                                .expect(200)
                                .expect('Content-Type', /json/)
                                .then(({ body }) => {
                                    assert.ok(Date.parse(body.created) < futureDate);
                                });
                        });
                });

                it('Возвращает 400, если передать невалидный reference', () => (
                    authenticatedUser
                        .get('/v1/blog/drafts')
                        .then((response) => {
                            const { body } = response;
                            // eslint-disable-next-line
                            const postId = body.list[0]._id;

                            return authenticatedUser
                                .patch(`/v1/blog/posts/${postId}`)
                                .send({ reference: 13 })
                                .set('Content-Type', 'application/json')
                                .expect(400);
                        })
                ));
            });
        });

        describe('DELETE /v1/blog/posts/:id', () => {
            describe('Позитивные', () => {
                it('Удаляет пост и файлы внутри', () => (
                    authenticatedUser
                        .post('/v1/blog/images')
                        .attach('photo', path.join(__dirname, 'fixtures', 'cake.jpg'))
                        .then(({ body }) => (
                            authenticatedUser
                                .post('/v1/blog/posts')
                                // eslint-disable-next-line
                                .send({ files: [body._id] })
                                .set('Content-Type', 'application/json')
                        ))
                        .then(({ body }) => (
                            authenticatedUser
                                // eslint-disable-next-line
                                .patch(`/v1/blog/posts/${body._id}`)
                                .send({ draft: false })
                                .set('Content-Type', 'application/json')
                        ))
                        .then(({ body }) => (
                            authenticatedUser
                                // eslint-disable-next-line
                                .delete(`/v1/blog/posts/${body._id}`)
                                .then(({ body: post }) => (
                                    authenticatedUser
                                        // eslint-disable-next-line
                                        .get(`/v1/blog/posts/${post._id}`)
                                        .expect(404)
                                ))
                                .then(() => (
                                    authenticatedUser
                                        .get(`/v1/blog/images/${body.files[0]}`)
                                ))
                        ))
                        .then(({ body: file }) => {
                            assert.strictEqual(file.deleted, true);
                        })
                ));
            });

            describe('Негативные', () => {
                it('Возвращает 404, если поста нет', () => (
                    authenticatedUser
                        .delete('/v1/blog/posts/5c75b29a114e8bd3d1f40229')
                        .expect(404)
                ));

                it('Возвращает 400, если передать не MongoId', () => (
                    authenticatedUser
                        .delete('/v1/blog/posts/123123')
                        .expect(400)
                ));

                it('Возвращает 401, если стучится незареганный пользователь', () => (
                    request(app)
                        .delete('/v1/blog/posts/123123')
                        .expect(401)
                ));
            });
        });

        describe('GET /v1/blog/posts/slugs/:slug', () => {
            describe('Позитивные', () => {
                it('Возвращает слаг', () => (
                    authenticatedUser
                        .get('/v1/blog/posts/slugs/moy-slag')
                        .expect(200)
                ));
            });

            describe('Негативные', () => {
                it('Возвращает 401, если стучится незареганный пользователь', () => (
                    request(app)
                        .get('/v1/blog/posts/slugs/moy-slag')
                        .expect(401)
                ));

                it('Возвращает 404, если слага нет', () => (
                    authenticatedUser
                        .get('/v1/blog/posts/slugs/ld2kas2d21mq3o1i')
                        .expect(404)
                ));
            });
        });
    });
};

module.exports = postsTests;
