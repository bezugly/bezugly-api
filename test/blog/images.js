const path = require('path');
const fs = require('fs');
const assert = require('assert');
const request = require('supertest');

const draftsTests = function (app, authenticatedUser) {
    return (
        describe('Изображения', () => {
            describe('POST /v1/blog/images', () => {
                describe('Позитивные', () => {
                    it('Загружает изображение любого размера, записывает в базу данных и сохраняет файл в папку', () => (
                        authenticatedUser
                            .post('/v1/blog/images')
                            .attach('photo', path.join(__dirname, 'fixtures', '13mb-image.jpg'))
                            .then(({ body: file }) => {
                                assert.ok(file);
                                assert.strictEqual(
                                    fs.existsSync(path.join(process.env.UPLOADS_FOLDER, file.name)),
                                    true,
                                );
                            })
                    ));
                });

                describe('Негативные', () => {
                    it('Возвращает 401, если стучится незареганный пользователь', () => (
                        request(app)
                            .post('/v1/blog/images')
                            .attach('photo', path.join(__dirname, 'fixtures', 'cake.jpg'))
                            .expect(401)
                    ));

                    it('Возвращает 415, если Content-Type не multipart/form-data', () => (
                        authenticatedUser
                            .post('/v1/blog/images')
                            .set('Content-Type', 'text/plain')
                            .expect(415)
                    ));

                    it('Возвращает 415, если передать не изображение', () => (
                        authenticatedUser
                            .post('/v1/blog/images')
                            .set('Content-Type', 'multipart/form-data')
                            .attach('photo', path.join(__dirname, 'fixtures', 'text.txt'))
                            .expect(400)
                    ));

                    it('Не заменяет файл в папке, если передать существующее название', () => {
                        const originalFileName = 'cake.jpg';

                        return authenticatedUser
                            .post('/v1/blog/images')
                            .attach('photo', path.join(__dirname, 'fixtures', originalFileName))
                            .then(() => (
                                authenticatedUser
                                    .post('/v1/blog/images')
                                    .attach('photo', path.join(__dirname, 'fixtures', originalFileName))
                                    .then(({ body: file }) => {
                                        assert.ok(file.name !== originalFileName);
                                    })
                            ));
                    });
                });
            });

            describe('GET /v1/blog/images/:id', () => {
                describe('Позитивные', () => {
                    it('Отдает изображение', () => (
                        authenticatedUser
                            .post('/v1/blog/images')
                            .attach('photo', path.join(__dirname, 'fixtures', '13mb-image.jpg'))
                            .then(({ body }) => body)
                            .then(file => (
                                authenticatedUser
                                    // eslint-disable-next-line
                                    .get(`/v1/blog/images/${file._id}`)
                                    .expect(200)
                                    .expect('Content-Type', /json/)
                                    .then(({ body }) => {
                                        // eslint-disable-next-line
                                        assert.ok(body._id);
                                    })
                            ))
                    ));
                });
            });

            describe('PATCH /v1/blog/images/:id', () => {
                describe('Позитивные', () => {
                    it('Записывает изменения в изображении', () => (
                        authenticatedUser
                            .post('/v1/blog/images')
                            .attach('photo', path.join(__dirname, 'fixtures', '13mb-image.jpg'))
                            .then(({ body }) => body)
                            .then(file => (
                                authenticatedUser
                                    // eslint-disable-next-line
                                    .patch(`/v1/blog/images/${file._id}`)
                                    .send({
                                        name: 'Приветулик.jpg',
                                        deleted: true,
                                    })
                                    .set('Content-Type', 'application/json')
                                    .expect(200)
                                    .expect('Content-Type', /json/)
                                    .then(({ body: editedFile }) => {
                                        // eslint-disable-next-line
                                        assert.ok(editedFile._id === file._id);
                                    })
                            ))
                    ));
                });

                describe('Негативные', () => {
                    it('Возвращает 401, если стучится незареганный пользователь', () => (
                        request(app)
                            .patch('/v1/blog/images/5c79b02a114e8bd3d0f4052f')
                            .set('Content-Type', 'application/json')
                            .expect(401)
                    ));

                    it('Возвращает 415, если Content-Type не application/json', () => (
                        authenticatedUser
                            .patch('/v1/blog/images/5c79b02a114e8bd3d0f4052f')
                            .set('Content-Type', 'text/plain')
                            .expect(415)
                    ));

                    it('Возвращает 400, если передать не MongoId', () => (
                        authenticatedUser
                            .patch('/v1/blog/images/123')
                            .set('Content-Type', 'application/json')
                            .expect(400)
                    ));

                    it('Возвращает 404, если файла нет', () => (
                        authenticatedUser
                            .patch('/v1/blog/images/5c79b02a114e8bd3d0f4052f')
                            .set('Content-Type', 'application/json')
                            .expect(404)
                    ));

                    it('Возвращает 400, если попробовать изменить id поста', () => (
                        authenticatedUser
                            .patch('/v1/blog/images/5c79b02a114e8bd3d0f4052f')
                            .send({ _id: '5c75ba5a190bfcd40853747f' })
                            .set('Content-Type', 'application/json')
                            .expect(400)
                    ));
                });
            });
        })
    );
};

module.exports = draftsTests;
