const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');
const mongoose = require('mongoose');
const MongoStore = require('connect-mongo')(session);
const expressValidator = require('express-validator');
const morgan = require('morgan');
const passport = require('passport');
const cors = require('cors');
const moment = require('moment-timezone');
const logger = require('./handlers/logger');

const blogRouter = require('./routes/blog');
const userRouter = require('./routes/user');

const errorHandlers = require('./handlers/errorHandlers');
require('./handlers/passport');

const app = express();

app.use(bodyParser.json({ limit: '10mb', extended: true }));
app.use(bodyParser.urlencoded({ extended: true }));

if (process.env.NODE_ENV !== 'test') {
    morgan.token('moscow-date', () => moment().tz('Europe/Moscow').format('YYYY-MM-DD HH:mm:ss'));

    app.use(morgan(':moscow-date - :method :url :status - HTTP/:http-version - :user-agent - :remote-addr - :remote-user'));
}

app.use(cors({ origin: process.env.CORS_ALLOWED_ORIGIN }));

app.use(expressValidator());

app.use(session({
    secret: process.env.SECRET,
    key: process.env.KEY,
    resave: false,
    saveUninitialized: false,
    store: new MongoStore({ mongooseConnection: mongoose.connection }),
}));

app.use((req, res, next) => {
    res.locals.env = app.get('env');

    next();
});

app.use(passport.initialize());
app.use(passport.session());

app.use('/uploads/blog', express.static(process.env.UPLOADS_FOLDER));

app.use('/', (req, res, next) => {
    logger.info(`${req.method} ${req.originalUrl} - ${req.ip}`);

    if (req.url === '/') {
        res.sendFile(path.join(__dirname, 'index.html'));
    } else {
        next();
    }
});

app.use('/v1/blog', blogRouter);
app.use('/v1/user', userRouter);

app.use(errorHandlers.notFound);

module.exports = app;
