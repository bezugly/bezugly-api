const mongoose = require('mongoose');

const { Schema } = mongoose;
const validator = require('validator');
const mongodbErrorHandler = require('mongoose-mongodb-errors');
const passportLocalMongoose = require('passport-local-mongoose');

mongoose.Promise = global.Promise;

const userSchema = new Schema({
    email: {
        type: String,
        unique: true,
        lowercase: true,
        trim: true,
        validate: [validator.isEmail, 'Invalid Email Address'],
        required: 'Please supply an email address',
    },
    name: {
        type: String,
        trim: true,
        required: 'Please supply a name',
    },
});

userSchema.plugin(passportLocalMongoose, { usernameField: 'email' });
// TODO: Проверить, как будет с ним и без него
userSchema.plugin(mongodbErrorHandler);

module.exports = mongoose.model('User', userSchema);
