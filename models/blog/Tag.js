const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

const tagSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        unique: true,
        required: 'Please enter a tag name',
    },
});

module.exports = mongoose.model('Tag', tagSchema);
