const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
const slug = require('slugs');
const translit = require('translit')(require('translit-russian'));

const postSchema = new mongoose.Schema({
    title: {
        type: String,
        trim: true,
    },
    content: {
        type: String,
        trim: true,
    },
    lead: {
        type: String,
        trim: true,
    },
    draft: {
        type: Boolean,
        default: true,
    },
    // Слаг черновика. Чтобы не путались айдишники при обновлении
    draftSlug: String,
    // Слаг для людей
    slug: String,
    // ID скопированного поста. Если отредактировать существующий пост,
    // то мы клонируем его и записываем reference, чтобы потом заменить
    reference: mongoose.Schema.Types.ObjectId,
    tags: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Tag',
        },
    ],
    files: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'File',
        },
    ],
    ogImage: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'File',
    },
    created: {
        type: Date,
        default: Date.now,
    },
    updated: {
        type: Date,
        default: Date.now,
    },
});

postSchema.index({ title: 'text', content: 'text' }, { weights: { title: 2, content: 1 } });

postSchema.pre('save', async function (next) {
    // Редактирование слага черновика
    if (!this.draftSlug) {
        this.draftSlug = 'sweet-fantasy';
    }

    this.draftSlug = slug(translit(this.draftSlug));

    // Редактирование слага
    if (!this.slug) {
        this.slug = this.draftSlug;
    }

    this.slug = slug(translit(this.slug));

    const { _id: id } = this;
    const draftSlugRegEx = new RegExp(`^(${this.draftSlug})((-[0-9]*$)?)$`, 'i');
    const postsWithDraftSlug = await this.constructor.find({
        draftSlug: draftSlugRegEx,
        _id: { $ne: id },
    });

    if (postsWithDraftSlug.length) {
        const slugWithoutNumbers = this.draftSlug.replace(/((-[0-9]*$)?)$/, '');
        this.draftSlug = `${slugWithoutNumbers}-${postsWithDraftSlug.length + 1}`;
    }

    // Редактирование названия слага, только если пост для пользователей
    if (!this.draft) {
        const slugRegEx = new RegExp(`^(${this.slug})((-[0-9]*$)?)$`, 'i');
        const postsWithSlug = await this.constructor.find({
            slug: slugRegEx,
            draft: false,
            _id: { $ne: id },
        });

        if (postsWithSlug.length) {
            const slugWithoutNumbers = this.slug.replace(/((-[0-9]*$)?)$/, '');
            this.slug = `${slugWithoutNumbers}-${postsWithSlug.length + 1}`;
        }
    }

    // Редактирование даты
    if (Date.parse(this.created) > Date.now()) {
        this.created = new Date().toISOString();
    }

    this.updated = new Date().toISOString();

    next();
});

module.exports = mongoose.model('Post', postSchema);
