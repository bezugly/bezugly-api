const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

const fileSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
    },
    originalName: {
        type: String,
        trim: true,
    },
    mimeType: String,
    src: {
        type: String,
        trim: true,
    },
    deleted: {
        type: Boolean,
        default: false,
    },
});

fileSchema.index({ filename: 'text' });

module.exports = mongoose.model('File', fileSchema);
