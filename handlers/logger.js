const appRoot = require('app-root-path');
const { createLogger, format, transports } = require('winston');

const defaultSettings = {
    handleExceptions: true,
    json: true,
    maxsize: 5242880, // 5MB
    maxFiles: 5,
    colorize: false,
};
const options = {
    info: {
        ...defaultSettings,
        level: 'info',
        filename: `${appRoot}/logs/app.log`,
    },
    error: {
        ...defaultSettings,
        level: 'error',
        filename: `${appRoot}/logs/app-error.log`,
    },
};

const logger = createLogger({
    format: format.combine(
        format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss',
        }),
        format.simple(),
    ),
    transports: [
        new transports.File(options.info),
        new transports.File(options.error),
    ],
});

if (process.env.NODE_ENV === 'development') {
    logger.add(new transports.Console({
        format: format.combine(
            format.colorize(),
            format.simple(),
        ),
    }));
}

module.exports = logger;
