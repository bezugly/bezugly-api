const mongoose = require('mongoose');
const AppError = require('../errors/AppError');
const logger = require('./logger');

const Post = mongoose.model('Post');

const catchErrors = fn => (req, res, next) => fn(req, res, next).catch(next);

const sendError = ({ status = 500, message = '', errors = [] }, req, res) => {
    // const hasStack = Boolean(req.locals && req.locals.env === 'development');
    const error = new AppError(status, message, errors);
    const loggerMessage = `${new Date()} - ${error.status} - ${error.message} - ${req.method} ${req.originalUrl} - ${req.ip}`;

    if (error.status === 500) {
        logger.error(loggerMessage);
    } else {
        logger.warn(loggerMessage);
    }

    res.status(error.status);
    res.json(error);
};

const validateContentType = contentType => (req, res, next) => {
    const regExp = new RegExp(contentType);

    if (!regExp.test(req.headers['content-type'])) {
        sendError({
            status: 415,
            message: 'Неподдерживаемый content-type',
        }, req, res);
    } else {
        next();
    }
};

const validateFiles = (req, res, next) => {
    const { files = [] } = req.body;
    const isValid = files.every(id => mongoose.Types.ObjectId.isValid(id.toString()));

    if (!isValid) {
        return sendError({ status: 400, message: 'Невалидные файлы' }, req, res);
    }

    return next();
};

const validateReference = async (req, res, next) => {
    const { reference: ref = '' } = req.body;
    const reference = ref.toString();

    if (!reference.length) {
        return next();
    }

    const isValid = mongoose.Types.ObjectId.isValid(reference);

    if (!isValid) {
        return sendError({ status: 400, message: 'Невалидная ссылка на родительский пост' }, req, res);
    }

    const post = await Post.findOne({ _id: reference, draft: false });

    if (!post) {
        return sendError({ status: 400, message: `Родительского поста с id ${reference} не существует` }, req, res);
    }

    return next();
};

const validateMongoId = (req, res, next) => {
    const { id } = req.params;

    if (!mongoose.Types.ObjectId.isValid(id.toString())) {
        return sendError({ status: 400, message: 'Невалидный id' }, req, res);
    }

    return next();
};

const notFound = (req, res) => sendError({ status: 404, message: 'Нет такого роута' }, req, res);

module.exports = {
    catchErrors,
    validateContentType,
    validateMongoId,
    validateFiles,
    validateReference,
    notFound,
    sendError,
};
