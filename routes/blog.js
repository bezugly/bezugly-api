const express = require('express');

const blog = express.Router();
const blogController = require('../controllers/blogController');
const { isLoggedIn } = require('../controllers/userController');
const {
    catchErrors,
    validateContentType,
    validateMongoId,
    validateFiles,
    validateReference,
} = require('../handlers/errorHandlers');

/**
 * Список постов
 */
blog.get('/posts', catchErrors(blogController.getPosts));

/**
 * Создать пост и записать в черновики
 */
blog.post('/posts', isLoggedIn, [
    validateContentType('application/json'),
    validateFiles,
    validateReference,
    catchErrors(blogController.createPost),
]);

/**
 * Пост по айди
 */
blog.get('/posts/:id', validateMongoId, catchErrors(blogController.getPost));

/**
 * Записать изменения в посте
 * Вернуть пост в черновики
 * Опубликовать пост
 */
blog.patch('/posts/:id', isLoggedIn, [
    validateContentType('application/json'),
    validateMongoId,
    validateFiles,
    validateReference,
    catchErrors(blogController.editPost),
]);

/**
 * Удалить пост
 */
blog.delete('/posts/:id', isLoggedIn, [
    validateMongoId,
    catchErrors(blogController.removePost),
]);

/**
 * Проверка слага на уникальность
 */
blog.get('/posts/slugs/:slug', isLoggedIn, catchErrors(blogController.getPostSlug));

/**
 * Список черновиков
 */
blog.get('/drafts', isLoggedIn, catchErrors(blogController.getDrafts));

/**
 * Черновик по айди
 */
blog.get('/drafts/:id', isLoggedIn, validateMongoId, catchErrors(blogController.getDraft));

/**
 * Список тегов
 */
// blog.get('/tags', catchErrors(blogController.getTags));

/**
 * Список постов под тегом
 */
// blog.get('/tags/:id', catchErrors(blogController.getPostsByTag));

/**
 * Загрузить фото
 */
blog.post('/images', isLoggedIn, [
    validateContentType('multipart/form-data'),
    blogController.uploadImage,
    catchErrors(blogController.createFile),
]);

/**
 * Файл по айди
 */
blog.get('/images/:id', catchErrors(blogController.getFile));

/**
 * Записать изменения в файл
 */
blog.patch('/images/:id', isLoggedIn, [
    validateContentType('application/json'),
    validateMongoId,
    catchErrors(blogController.editFile),
]);

/**
 * Искать по тексту
 */
// blog.get('/search', catchErrors(blogController.searchPost));

module.exports = blog;
