const express = require('express');

const router = express.Router();
const userController = require('../controllers/userController');
const { catchErrors, validateContentType } = require('../handlers/errorHandlers');

if (process.env.NODE_ENV !== 'production') {
    router.post('/register', [
        validateContentType('application/json'),
        userController.validateRegister,
        catchErrors(userController.register),
        userController.login,
    ]);
}

router.post('/login', [
    validateContentType('application/json'),
    userController.validateLogin,
    userController.login,
]);

router.get('/logout', userController.logout);

router.get('/', userController.getCurrentUser);

module.exports = router;
