class AppError extends Error {
    constructor(httpCode, message, errors) {
        super();

        Error.captureStackTrace(this);

        this.status = httpCode;
        this.message = message;
        this.errors = errors;
    }
}

module.exports = AppError;
