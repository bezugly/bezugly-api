require('dotenv').config();

const mongoose = require('mongoose');
const logger = require('./handlers/logger');

mongoose.Promise = global.Promise;
mongoose.connect(process.env.DATABASE, { useNewUrlParser: true });
mongoose.connection.on('error', (error) => {
    logger.error(`MongoDb Error → ${error}, PID ${process.pid}`);
});

require('./models/blog/Post');
require('./models/blog/Tag');
require('./models/blog/File');
require('./models/User');

const app = require('./app');

app.listen(process.env.PORT, () => {
    logger.info(`Express running → PORT ${process.env.PORT}, PID ${process.pid}`);
});
